package net.sourceforge.goalseeker.fifteenPuzzle.swing;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import net.sourceforge.goalseeker.core.NodeState;
import net.sourceforge.goalseeker.core.swing.PuzzleJPanel;
import net.sourceforge.goalseeker.fifteenPuzzle.FifteenNodeState;
/**
 * This panel implements the display of the current node state. It also allows a
 * user to click on a block in order to move the block to the free space.
 * 
 * @author: Darius Cooper
 */
public class FifteenPanel extends PuzzleJPanel
{
  private static final Color COLOR_BUTTONS = new Color(60, 165, 170);
  private static final Color COLOR_FREE = new Color(170, 180, 254);
  // The buttons that fill the grid
  private JButton[][] buttons = new JButton[FifteenNodeState.GRID_SIZE][FifteenNodeState.GRID_SIZE];
  private String imageFileName = "c://temp//testtile.png";
  private ImageIcon[] buttonIcons = new ImageIcon[FifteenNodeState.GRID_SIZE
      * FifteenNodeState.GRID_SIZE];

  /**
   * Default constructor. Sets up the buttons without any specific numbers,
   * because there is no <code>NodeState</code> to represent.
   */
  public FifteenPanel()
  {
    this.setLayout(new java.awt.GridLayout(FifteenNodeState.GRID_SIZE,
        FifteenNodeState.GRID_SIZE));
    for (int i = 0; i < FifteenNodeState.GRID_SIZE; i++)
    {
      for (int j = 0; j < FifteenNodeState.GRID_SIZE; j++)
      {
        buttons[i][j] = (new JButton());
        buttons[i][j].setMargin(new Insets(0,0,0,0));
        buttons[i][j].setFont(new Font("Arial", Font.BOLD, 32));
        buttons[i][j].addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent evt)
          {
            handleButtonClick((JButton) evt.getSource());
          }
        });
        this.add(buttons[i][j]);
      }
    }
    loadImages();
  }

  private void loadImages()
  {
    ImageIcon theImage = new ImageIcon("./images/tile_blank.jpg");

    theImage.setImage(theImage.getImage().getScaledInstance(70, -1, Image.SCALE_FAST));
    
    buttonIcons[0] = theImage;
    for (int i = 1; i <= 15; i++)
    {
      theImage = new ImageIcon("./images/tile_" + i+".jpg");

      theImage.setImage(theImage.getImage().getScaledInstance(70, -1, Image.SCALE_FAST));
      
      buttonIcons[i] = theImage;
    }
    // Image croppedImage = createImage(new FilteredImageSource(theImage
    // .getImage().getSource(), new CropImageFilter(73, 63, 141, 131)));
  }

  /**
   * Constructor to set up the panel and display the <code>FifteenNode</code>
   * that is provided.
   * 
   * @param nodeState
   *          The <code>FifteenNode</code> node state to be displayed.
   */
  public FifteenPanel(FifteenNodeState nodeState)
  {
    this();
    this.nodeState = nodeState;
    update(nodeState);
  }

  /**
   * Handles the <code>ActionEvent</code> of a user clicking on a button in
   * the panel, in order to move a block. If the move was a valid one, this
   * method will also trigger a <code>ValidUserMoveEvent</code>.
   * 
   * @param clickedButton
   *          The button that was clicked.
   */
  public void handleButtonClick(JButton clickedButton)
  {
    // If the panel is currently set to "Display-only" mode, ignore the click
    if (isDisplayOnly())
    {
      return;
    }
    // Identify the grid-location of the clicked button
    // To-do: This is something that can be optimized in a later release
    for (int i = 0; i < FifteenNodeState.GRID_SIZE; i++)
    {
      for (int j = 0; j < FifteenNodeState.GRID_SIZE; j++)
      {
        if (buttons[i][j] == clickedButton)
        {
          try
          {
            ((FifteenNodeState) this.nodeState).moveBlock(i, j);
            update(this.nodeState);
            if (validUserMoveListener != null)
            {
              validUserMoveListener
                  .validUserMovePerformed(this.nodeState);
            }
          } catch (NodeState.InvalidOperationException e1)
          {
            try
            {
              Thread.sleep(MILLISECONDS_INVALID_OPERATION);
            } catch (InterruptedException e2)
            {
            }
          }
        }
      }
    }
  }

  /**
   * Updates a new state to be displayed in the panel.
   */
  public void update()
  {
    for (int i = 0; i < FifteenNodeState.GRID_SIZE; i++)
    {
      for (int j = 0; j < FifteenNodeState.GRID_SIZE; j++)
      {
        if (((FifteenNodeState) nodeState).isFreeBlock(i, j))
        {
          //buttons[i][j].setText("");
          //buttons[i][j].setBackground(COLOR_FREE);
          buttons[i][j].setIcon(buttonIcons[0]);
        }
        else
        {
        //  buttons[i][j].setText(Integer.toString(((FifteenNode) nodeState)
        //      .getBlockNum(i, j)));
          //buttons[i][j].setBackground(COLOR_BUTTONS);
          buttons[i][j].setIcon(buttonIcons[((FifteenNodeState) nodeState)
              .getBlockNum(i, j)]);

        }
      }
    }
    // buttons[0][0].setIcon(new ImageIcon(buttonIcons[0][0].getImage()
    // .getScaledInstance(buttons[0][0].getWidth(), buttons[0][0].getHeight(),
    // Image.SCALE_SMOOTH)));
    invalidate();
    repaint();
  }

  public static void main(String[] args)
  {
    FifteenNodeState testState = new FifteenNodeState(20);
    // SearchNode startNode = new SearchNode(testState);
    // Heuristic heuristic = new FifteenHeuristic();
    FifteenPanel testPanel = new FifteenPanel(testState);
    JFrame frame = new JFrame();
    frame.getContentPane().add(testPanel);
    frame.setSize(300, 300);
    frame.setVisible(true);
  }
}