package net.sourceforge.goalseeker.fifteenPuzzle.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.sourceforge.goalseeker.core.NodeState;
import net.sourceforge.goalseeker.core.PuzzleService;
import net.sourceforge.goalseeker.core.swing.PuzzleJPanel;
import net.sourceforge.goalseeker.core.swing.ValidUserMoveListener;
import net.sourceforge.goalseeker.fifteenPuzzle.FifteenHeuristic;
import net.sourceforge.goalseeker.fifteenPuzzle.FifteenNodeState;

/**
 * This class "puts it all together". It is the "driver" that uses the other
 * classes to implement a usable fifteen puzzle.
 * 
 * @author: Darius Cooper
 */
public class FifteenApplet extends JApplet implements ValidUserMoveListener
{

  // Color for the text
  private static final Color COLOR_TEXT = new Color(25, 50, 20);
  // Color to use for the command buttons (RHS panel)
  protected static final Color COLOR_COMMANDS = new Color(150, 200, 255);
  // Color to use for the main panel background
  private static final Color COLOR_MAIN_BACKGROUND = new Color(50, 115, 115);

  // Strictly, we don't require our own button class, but I intend to
  // add more interesting functionality to this class in a later release.
  private class CommandButton extends javax.swing.JButton
  {

    CommandButton()
    {
      super();
      setBackground(COLOR_COMMANDS);
    }

    CommandButton(String text)
    {
      super(text);
      setBackground(COLOR_COMMANDS);
      setBorder(BorderFactory.createRaisedBevelBorder());
    }
  }

  private PuzzleService puzzleService = new PuzzleService();
  private PuzzleJPanel fifteenPanel = new FifteenPanel();
  
  // Various display components
  private JFrame frame = new JFrame();
  private JPanel mainPanel = new JPanel();
  private JPanel fifteenPanelHolder = new JPanel(new BorderLayout());
  private Box mainHBox = Box.createHorizontalBox();
  private JPanel commandPanel = new JPanel(new GridLayout(8, 1));

  private Box movesHBox = Box.createHorizontalBox();
  private Box movesVBox = Box.createVerticalBox();
  private JLabel movesLabel = new JLabel("Moves: ");
  private JLabel movesCountLabel = new JLabel("0");
  private Box resetHBox = Box.createHorizontalBox();
  private CommandButton resetCountButton = new CommandButton("Reset Counter");
  private Box shuffleHBox = Box.createHorizontalBox();
  private CommandButton shuffleButton = new CommandButton("Shuffle");
  private Box solveHBox = Box.createHorizontalBox();
  private CommandButton solveButton = new CommandButton("Solve");
  private CommandButton nextStepButton = new CommandButton("Next Step >>");
  private CommandButton undoButton = new CommandButton("Undo");

  /**
   * Constructor. If using a main() , call <code>init()</code> after
   * constructing the <code>Puzzle</code> object.
   */
  public FifteenApplet()
  {
    super();
  }

  /**
   * Handles the users's request to see the next step in the solution.
   */
  protected void handleNextStep()
  {
    puzzleService.handleNextStep();
    updateDisplay();
  }

  /**
   * Handles the users's request reset the counter that is being displayed. It
   * does not reset the state of the puzzle itself (currently, that can be done
   * by a series of UN-do requests).
   */
  protected void handleResetCount()
  {
    puzzleService.handleResetCount();
    updateDisplay();
  }

  /**
   * 
   */
  private void updateDisplay()
  {
    if (puzzleService.getCurrentNodeState() != null)
    {
      fifteenPanel.update(puzzleService.getCurrentNodeState());
    }
    movesCountLabel.setText(puzzleService.getDisplayedMoveNum());
    // if (puzzleState.isAtSolutionNode())
    if (puzzleService.getCurrentNodeState().isGoalState())
    {
      nextStepButton.setEnabled(false);
      JOptionPane.showMessageDialog(frame, "Solved!", "Solved!",
          JOptionPane.INFORMATION_MESSAGE);

      fifteenPanel.setDisplayOnly(false);
    }
    frame.invalidate();
    frame.repaint();

  }

  /**
   * 
   * Invoked when a user makes a valid move in the <code>FifteenPanel</code>.
   * It ensures that the other elements in the display (e.g. counters and
   * buttons are modified in accordance with the new state of the puzzle)
   * 
   * @param event A ValidMoveEvent trigger by a user clicking on a button.
   */
  @Override
  public void validUserMovePerformed(NodeState newNodeState)
  {
    //First pass the event up to the Puzzle-State
    puzzleService.validUserMovePerformed(newNodeState);
    
    movesCountLabel.setText(Integer.toString(puzzleService.getPlayerMoveIdx()));
    undoButton.setEnabled(true);
    if (puzzleService.getCurrentNodeState().isGoalState())
    {
      JOptionPane.showMessageDialog(frame, "You did it!", "Congrats!",
          JOptionPane.INFORMATION_MESSAGE);
    }
    frame.invalidate();
    frame.repaint();
  }
  
  /**
   * Handles the users's request to shuffle the puzzle. It creates a new
   * start-state This means that a new computer-figured solution is also
   * computed.
   */
  protected void handleShuffle()
  {
    if (puzzleService.isBusySearching())
    {
      puzzleService.stopSolving();
      try
      {
        Thread.sleep(1000);
      }
      catch (InterruptedException ex)
      {
      }
      JOptionPane.showMessageDialog(frame, "Please wait....  closing previous solution",
          "Closing...", JOptionPane.INFORMATION_MESSAGE);
    }
    puzzleService.waitForSearchCompletion();
    setUpInitialState();
    solveButton.setEnabled(true);
    nextStepButton.setEnabled(false);
    handleResetCount();
    frame.invalidate();
    frame.repaint();
  }

  /**
   * Handles the users's request to see the solution. It restores the
   * start-state of the puzzle. The user can then step through the solution with
   * the "Next Step" button.
   */
  protected void handleSolve()
  {

    if (puzzleService.isBusySearching())
    {
      JOptionPane.showMessageDialog(frame, "Please wait....  solving puzzle",
          "Solving...", JOptionPane.INFORMATION_MESSAGE);
    }
    puzzleService.retrieveSolution();

    updateDisplay();
    JOptionPane
        .showMessageDialog(
            frame,
            "Starting from initial state.\nThen, use <NextStep> to walk through the solution",
            "Starting!", JOptionPane.INFORMATION_MESSAGE);
    fifteenPanel.setDisplayOnly(true);
    fifteenPanel.update(puzzleService.getCurrentNodeState());
    solveButton.setEnabled(false);
    nextStepButton.setEnabled(true);
    frame.invalidate();
    frame.repaint();
  }

  /**
   * Handles the users's request to undo a user move. While undoing the move,
   * the count is also decremented, to truly allow the user to "take back" the
   * move and not have it be counted as part of the attempted moves.
   */
  protected void handleUndo()
  {
    puzzleService.handleUndo();
    updateDisplay();
  }

  /**
   * Initializes the state of the puzzle and then sets up the display.
   */
  public void init()
  {
    setUpInitialState();
    resetCountButton.setBackground(COLOR_COMMANDS);
    resetCountButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        handleResetCount();
      }
    });
    shuffleButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        handleShuffle();
      }
    });
    solveButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        handleSolve();
      }
    });
    nextStepButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        handleNextStep();
      }
    });
    undoButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        handleUndo();
      }
    });
    handleResetCount();
    // resetCountButton.setMinimumSize(new Dimension(200, 50));
    movesLabel.setForeground(COLOR_TEXT);
    movesCountLabel.setForeground(COLOR_TEXT);
    movesHBox.add(Box.createHorizontalStrut(5));
    movesHBox.add(movesLabel);
    movesHBox.add(movesCountLabel);
    movesVBox.add(movesHBox);
    resetHBox.add(resetCountButton);
    resetHBox.add(Box.createHorizontalGlue());
    movesVBox.add(resetHBox);
    // shuffleButton.setMinimumSize(new Dimension(200, 50));
    shuffleHBox.add(shuffleButton);
    shuffleHBox.add(Box.createHorizontalGlue());
    // solveButton.setMinimumSize(new Dimension(200, 50));
    solveHBox.add(solveButton);
    solveHBox.add(Box.createHorizontalStrut(5));
    // nextStepButton.setMinimumSize(new Dimension(200, 50));
    solveHBox.add(nextStepButton);
    nextStepButton.setEnabled(false);
    solveHBox.add(Box.createHorizontalGlue());
    undoButton.setEnabled(false);
    commandPanel.setBackground(COLOR_COMMANDS);
    commandPanel.add(movesHBox);
    commandPanel.add(resetCountButton);
    commandPanel.add(undoButton);
    commandPanel.add(shuffleButton);
    commandPanel.add(solveButton);
    commandPanel.add(nextStepButton);
    commandPanel.add(new JPanel());
    commandPanel.add(new JPanel());
    fifteenPanelHolder.add(fifteenPanel);
    mainHBox.add(Box.createHorizontalStrut(2));
    mainHBox.add(fifteenPanelHolder);
    fifteenPanel.addValidUserMoveListener(this);
    mainHBox.add(Box.createHorizontalStrut(3));
    mainHBox.add(commandPanel);
    mainHBox.add(Box.createHorizontalStrut(2));
    mainHBox.add(Box.createHorizontalGlue());
    mainPanel.setBackground(COLOR_MAIN_BACKGROUND);
    mainPanel.setLayout(new BorderLayout());
    mainPanel.add(mainHBox);
    frame.addWindowListener(new java.awt.event.WindowAdapter()
    {

      public void windowClosing(java.awt.event.WindowEvent evt)
      {
        if (puzzleService.isBusySearching())
        {
          puzzleService.stopSolving();
        }
      }
    });
    frame.getContentPane().add(mainPanel);
    frame.setSize(400, 200);
    frame.setVisible(true);
    JOptionPane.showMessageDialog(frame, "Click on a block to move it.",
        "Try to solve this...", JOptionPane.INFORMATION_MESSAGE);
  }

  /**
   * Creates a <code>Puzzle</code> object and runs it.
   */
  public static void main(String[] args)
  {
    FifteenApplet puzzle = new FifteenApplet();
    puzzle.init();
  }

  /**
   * Sets up the initial state of the puzzle. This involves creating a new
   * <code>NodeState</code> and spawning a search for a solution (in a
   * separate <code>Thread</code>.
   */
  private void setUpInitialState()
  {
    puzzleService.setUpInitialState(new FifteenNodeState(20), new FifteenHeuristic());
    fifteenPanel.update(puzzleService.getCurrentNodeState());
    fifteenPanel.setDisplayOnly(false);

  }



}