package net.sourceforge.goalseeker.fifteenPuzzle;

import net.sourceforge.goalseeker.core.Heuristic;

/**
 * Implements the heuristic that estimates the cost to get 
 * from the existing state to the solution state.
 * Cost is expressed as an integer which is a guess of the number
 * of operations required to get to the goal.
 * 
 * @author: Darius Cooper
 */
public class FifteenHeuristic implements Heuristic {
/**
 * FifteenHeuristic1 constructor. 
 */
public FifteenHeuristic() {
	super();
}
/**
 * Computes a heuristic that represents a guess of how close
 * the input state is to the goal state. 
 * 
 * This heuristic is designed to be optimistic. 
 * That means that it always guesses a cost that is
 * less than or equal to the actual cost to get to the goal.
 * 
 * The heuristic is computed like this: take each block, from 1 through 15
 * For each block, calculate the minimum number of moves that would be required
 * to bring it to its final place in the solution state, without taking into
 * account any intervening blocks. 
 * For instance, the '1' should be in the top-left position in the final goal-state.
 * If it is actually in the 3rd column of the second row, then it has to move 
 * up at least once (to get to the first row), and it has to move left at least twice
 * (to get to the first column) we add these two distances together and get a
 * total of 3 moves. In fact, it would take more moves, because there would be
 * some moves required to take the oter blocks out of the way. This computation
 * of distance to be travelled assuming a grid-like path, is sometimes referred to
 * as 'Manhattan distance' (as opposed to an 'as the crow flies' distance).
 *
 * We compute this distance for each block. The sum of such distances added across
 * all 15 blocks gives us an optimistic estimate of the total number of moves to
 * get to the deried solution.
 *
 * We assume that the solution state is on where 1,2,3 and 4 are in the first row;
 * 5,6,7,8 in the second; 9,10,11,12 in the third row,; and 13,14 and 15 in the
 * last row, starting at the first column. The last column of the last row is blank.
 *
 * @return A guess of the number of the minimum number of operations required 
 * to get to the goal
 *
 * @param currentNode aa.node.SearchNode
 */
public int getEstimatedCostToGoal(net.sourceforge.goalseeker.core.SearchNode currentNode) {

	int heuristicValue = 0;
	FifteenNodeState nodeState = (FifteenNodeState) currentNode.getNodeState();
	for (int i = 0; i < FifteenNodeState.GRID_SIZE; i++) {
		for (int j = 0; j < FifteenNodeState.GRID_SIZE; j++) {

			if (nodeState.isFreeBlock(i,j)) {
				//if (currentValue == FifteenNode.FREE_BLOCK_NUM) {
				continue;
			}
			int currentValue = nodeState.getBlockNum(i, j);
			int correctRow = (currentValue - 1) / FifteenNodeState.GRID_SIZE;
			int correctCol = (currentValue - 1) % FifteenNodeState.GRID_SIZE;
			heuristicValue += Math.abs(correctRow - i) + Math.abs(correctCol - j);

			//System.out.println("=== "+i+" "+j+" Heuristic= "+heuristicValue);
		}
	}
	return heuristicValue;
	//return 1;
}
}
