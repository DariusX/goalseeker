package net.sourceforge.goalseeker.fifteenPuzzle;
import net.sourceforge.goalseeker.core.NodeState;
/**
 * Represents the state of a "fifteen" puzzle. This puzzle is a 4 x 4 grid, with
 * 15 blocks (usually numbered 1,2...15) and one free space. One can slide the
 * blocks around the grid to change their positions. The objective of the puzzle
 * is to move the blocks into a predetermined pattern (or "state").
 * 
 * This class represents the state of the puzzle (i.e. the position of the
 * blocks) at any one point in time.
 * 
 * @author: Darius Cooper
 */
public class FifteenNodeState implements NodeState
{
  /**
   * The size of the grid. The grid is assume to be a square so a size of 3
   * means 3 x 3, a size of 4 means 4 x 4
   */
  public final static int GRID_SIZE = 4;

  
  private int costFromPrev = 0;
  // A grid that represents the 15 puzzle (or some other sized grid)
  private int[][] grid = new int[GRID_SIZE][GRID_SIZE];
  // The row in which the free-block exists (zero-relative)
  private int freeBlockRow = 0;
  // The column in which the free-block exists (zero-relative)
  private int freeBlockCol = 0;
  /**
   * The free block is numbered with a number that cannot be used for a
   * "legitimate" block. This variable allows the other
   */
  private final static int FREE_BLOCK_NUM = -9;

  /**
   * Default constructor currently assumes a 20-move shuffle
   */
  public FifteenNodeState()
  {
  }

  /**
   * Copy constructor.
   */
  public Object clone()
  {
    FifteenNodeState theNewNode = new FifteenNodeState();
    theNewNode.costFromPrev = costFromPrev;
    for (int i = 0; i < theNewNode.grid.length; i++)
    {
      for (int j = 0; j < theNewNode.grid[0].length; j++)
      {
        theNewNode.grid[i][j] = grid[i][j];
        theNewNode.freeBlockRow = freeBlockRow;
        theNewNode.freeBlockCol = freeBlockCol;
      }
    }
    return theNewNode;
  }

  /**
   * Compares two objects for equality. The results are consistent with the
   * <code>compare()</code> method.
   * 
   * @param obj
   *          the Object to compare with
   * @return true if these Objects are equal; false otherwise.
   * @see java.util.Hashtable
   */
  public boolean equals(Object obj)
  {
    if (!(obj instanceof FifteenNodeState))
    {
      return false;
    }
    return (compareTo(obj) == 0);
  }

  /**
   * Get the number (or identifier) of the block that is currently in a
   * particular position within the grid.
   * 
   * @return The number (or id) of the block. The caller must also call the
   *         <code>isFreeBlock(row,col)</code>
   * 
   * @param rowIdx
   *          int
   * @param colIdx
   *          int
   */
  public int getBlockNum(int rowIdx, int colIdx)
  {
    return grid[rowIdx][colIdx];
  }

  /**
   * Get the column number of the free-block (zero-relative)
   * 
   * @return The column number of the free block
   */
  private int getFreeBlockCol()
  {
    return freeBlockCol;
  }

  /**
   * Get the row number of the free-block (zero-relative)
   * 
   * @return The row number of the free block
   */
  private int getFreeBlockRow()
  {
    return freeBlockRow;
  }

  /**
   * Generates a hash code for the object.. This method is supported primarily
   * for hash tables, such as those provided in java.util.
   * 
   * @return an integer hash code for the receiver
   * @see java.util.Hashtable
   */
  public int hashCode()
  {
    return toString().hashCode();
  }

  /**
   * Checks if we are at a goal-state (or "solution state")
   * 
   * @return <code>true</code> if this is a goal state, else return
   *         <code>false</code>
   */
  public boolean isGoalState()
  {
    int goalBlockNum = 1;
    for (int i = 0; i < grid.length; i++)
    {
      for (int j = 0; j < grid[0].length; j++)
      {
        if ((this.getBlockNum(i, j) != FREE_BLOCK_NUM)
            && (this.getBlockNum(i, j) != goalBlockNum))
        {
          return false;
        }
        goalBlockNum++;
      }
    }
    return true;
  }

  /**
   * Returns a String that represents the value of this object.
   * 
   * @return a string representation of the receiver
   */
  public String toString()
  {
    String tempString = "{";
    for (int i = 0; i < grid.length; i++)
    {
      for (int j = 0; j < grid[0].length; j++)
      {
        tempString = tempString + " " + this.getBlockNum(i, j);
      }
      tempString = tempString + "  ";
    }
    tempString = tempString + "}";
    return tempString;
  }

  /**
   * FifteenNode constructor.
   * 
   * @param shuffleMoves
   *          The number of random moves (away from the solution state) to be
   *          performed while creathe the nodeState. See the
   *          <code>shuffle()</code> method for more details.
   */
  public FifteenNodeState(int shuffleMoves)
  {
    super();
    int blockNum = 1;
    for (int i = 0; i < grid.length; i++)
    {
      for (int j = 0; j < grid[0].length; j++)
      {
        if (blockNum < (GRID_SIZE * GRID_SIZE))
        {
          grid[i][j] = blockNum;
          blockNum++;
        }
        else
        {
          grid[i][j] = FREE_BLOCK_NUM;
          freeBlockRow = i;
          freeBlockCol = j;
        }
      }
    }
    shuffle(shuffleMoves);
    /**
     * * Commented code used for debugging ----------------------------
     * 
     * To check a particular starting node-state, fill in the details in the
     * grid array below and uncomment the rest of this commented block
     * 
     * grid = new int [][] {{15,14,13,9}, {-9,6,7,5}, {12,10,11,1}, {8,4,3,2}};
     * for (int i=0;i<grid.length;i++) { for (int j=0;j<grid[0].length;j++) {
     * if (grid[i][j] == FREE_BLOCK_NUM) { freeBlockRow = i; freeBlockCol = j; } } }
     * ********************-------------------------------**************
     */
  }

  /**
   * Implements the comparable interface. This allows <code>NodeState</code>
   * objects to be used in certain types of ordered collections (like a
   * <code>TreeSet</code>
   * 
   * @return int
   */
  public int compareTo(Object obj)
  {
    for (int i = 0; i < grid.length; i++)
    {
      for (int j = 0; j < grid[0].length; j++)
      {
        if (this.getBlockNum(i, j) < ((FifteenNodeState) obj).getBlockNum(i, j))
        {
          return -1;
        }
        else if (this.getBlockNum(i, j) > ((FifteenNodeState) obj).getBlockNum(i, j))
        {
          return +1;
        }
      }
    }
    return 0;
  }

  /**
   * Gets the list of <code>NodeState</code> objects that can be reached in a
   * single operation from the current node.
   * 
   * @return NodeState[] An array of FifteenNode objects.
   */
  public NodeState[] getNextNodeStateList()
  {
    FifteenNodeState tempNode = null;
    NodeState[] nodeCosts = new NodeState[4];
    int i = 0;
    tempNode = (FifteenNodeState) this.clone();
    try
    {
      tempNode.moveFreeLeft();
      tempNode.setCostFromPrev(1);
      nodeCosts[i++] = tempNode;
      tempNode = (FifteenNodeState) this.clone();
    } catch (InvalidOperationException e)
    {
    }
    try
    {
      tempNode.moveFreeRight();
      tempNode.setCostFromPrev(1);
      nodeCosts[i++] = tempNode;
      tempNode = (FifteenNodeState) this.clone();
    } catch (InvalidOperationException e)
    {
    }
    try
    {
      tempNode.moveFreeUp();
      tempNode.setCostFromPrev(1);
      nodeCosts[i++] = tempNode;
      tempNode = (FifteenNodeState) this.clone();
    } catch (InvalidOperationException e)
    {
    }
    try
    {
      tempNode.moveFreeDown();
      tempNode.setCostFromPrev(1);
      nodeCosts[i++] = tempNode;
    } catch (InvalidOperationException e)
    {
    }
    return nodeCosts;
  }

  /**
   * Checks if a particular position in the rid contaisn the
   * free-space/free-block
   * 
   * @return <code>true</code> if the position contains the free block, else
   *         return <code>false</code>
   * 
   * @param row
   *          Row position to check (zero-relative)
   * @param col
   *          Column position ton check (zero-relative)
   */
  public boolean isFreeBlock(int row, int col)
  {
    if ((getFreeBlockRow() == row) && (getFreeBlockCol() == col))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public void moveBlock(int i, int j) throws InvalidOperationException
  {
    if (this.getFreeBlockRow() == i)
    {
      if (this.getFreeBlockCol() + 1 == j)
      {
        this.moveFreeRight();
      }
      else if (this.getFreeBlockCol() - 1 == j)
      {
        this.moveFreeLeft();
      }
      else
      {
        throw new InvalidOperationException("Cannot move left or right");
      }
    }
    else if (this.getFreeBlockCol() == j)
    {
      if (this.getFreeBlockRow() + 1 == i)
      {
        this.moveFreeDown();
      }
      else if (this.getFreeBlockRow() - 1 == i)
      {
        this.moveFreeUp();
      }
      else
      {
        throw new InvalidOperationException("Cannot move up or down");
      }
    }
    else
    {
      throw new InvalidOperationException(
          "Cannot move unless is row or col of free space");
    }
  }

  public void moveFreeDown() throws InvalidOperationException
  {
    if (this.freeBlockRow != grid.length - 1)
    {
      this.grid[this.freeBlockRow][this.freeBlockCol] = this.grid[this.freeBlockRow + 1][this.freeBlockCol];
      // Set the freeblock to the RHS value
      this.freeBlockRow++; // Move freeblock down
      this.grid[this.freeBlockRow][this.freeBlockCol] = FREE_BLOCK_NUM;
    }
    else
    {
      throw new InvalidOperationException("Cannot move ");
    }
  }

  public void moveFreeLeft() throws InvalidOperationException
  {
    if (this.freeBlockCol != 0)
    {
      this.grid[this.freeBlockRow][this.freeBlockCol] = this.grid[this.freeBlockRow][this.freeBlockCol - 1];
      // Set the freeblock to the LHS value
      this.freeBlockCol--; // Move freeblock to the left
      this.grid[this.freeBlockRow][this.freeBlockCol] = FREE_BLOCK_NUM;
    }
    else
    {
      throw new InvalidOperationException("Cannot move left");
    }
  }

  /**
   * Insert the method's description here. Creation date: (2/15/2002 8:19:43 PM)
   */
  public void moveFreeRight() throws InvalidOperationException
  {
    if (this.freeBlockCol != grid[0].length - 1)
    {
      this.grid[this.freeBlockRow][this.freeBlockCol] = this.grid[this.freeBlockRow][this.freeBlockCol + 1];
      // Set the freeblock to the RHS value
      this.freeBlockCol++; // Move freeblock to the right
      this.grid[this.freeBlockRow][this.freeBlockCol] = FREE_BLOCK_NUM;
    }
    else
    {
      throw new InvalidOperationException("Cannot move left");
    }
  }

  public void moveFreeUp() throws InvalidOperationException
  {
    if (this.freeBlockRow != 0)
    {
      this.grid[this.freeBlockRow][this.freeBlockCol] = this.grid[this.freeBlockRow - 1][this.freeBlockCol];
      // Set the freeblock to the RHS value
      this.freeBlockRow--; // Move freeblock up
      this.grid[this.freeBlockRow][this.freeBlockCol] = FREE_BLOCK_NUM;
    }
    else
    {
      throw new InvalidOperationException("Cannot move left");
    }
  }

  /**
   * Shuffles the current state, by making a certain number of random moves.
   * Instead of trying to eliminate all cycles, this method prevents only
   * simplistic two-point cycles (e.g. "move left" immediately followed by "move
   * right") Longer cycles are possible, and some long random shuffles may even
   * bring the state all the way back to its original state!
   * 
   * @param times
   *          Number of random operations to attempt. Operations that are
   *          reversed (e.g. two-point cycles) are not counted.
   */
  public void shuffle(int times)
  {
    java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();
    long theSeed = calendar.getTime().getTime();
    System.out.println("Seed = " + theSeed);
    java.util.Random random = new java.util.Random(theSeed);
    int moveType = -1;
    int prevMoveType = -1;
    for (int i = 0; i < times; i++)
    {
      try
      {
        moveType = random.nextInt(GRID_SIZE);
        if (((moveType == 0) && (prevMoveType == 1))
            || ((moveType == 1) && (prevMoveType == 0))
            || ((moveType == 2) && (prevMoveType == 3))
            || ((moveType == 3) && (prevMoveType == 2)))
        {
          i--;
          continue;
        }
        switch (moveType)
        {
          case 0 :
            moveFreeLeft();
            break;
          case 1 :
            moveFreeRight();
            break;
          case 2 :
            moveFreeUp();
            break;
          case 3 :
            moveFreeDown();
            break;
        }
        prevMoveType = moveType;
      } catch (InvalidOperationException e)
      {
        i--;
      }
    }
  }

  public int getCostFromPrev()
  {
    return costFromPrev;
  }

  public void setCostFromPrev(int costFromPrev)
  {
    this.costFromPrev = costFromPrev;
  }
}