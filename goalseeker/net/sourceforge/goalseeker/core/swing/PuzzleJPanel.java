package net.sourceforge.goalseeker.core.swing;

import javax.swing.JPanel;

import net.sourceforge.goalseeker.core.NodeState;

/**
 * This panel implements the display of the current node state. It also allows a
 * user to click on a block in order to move the block to the free space.
 * 
 * @author: Darius Cooper 
 */
public abstract class PuzzleJPanel extends JPanel 
{

  public static final int MILLISECONDS_INVALID_OPERATION = 300;
  
  //The object that is listening for user moves to be performed
  protected ValidUserMoveListener validUserMoveListener = null;

  //Indicates that the user should not be able to change the state (it is
  //a "display only" state. However, programs may still change the state
  //via programming.
  private boolean displayOnly = false;

  //The <code>NodeState</code> object being displayed by panel
  protected NodeState nodeState = null;

  /**
   * Gets the <code>FifteenNode</code> currently being displayed by the panel.
   * 
   * @return The <code>FifteenNode</code> that is currently being displayed by
   *         the panel.
   */
  public NodeState getNodeState()
  {
    return nodeState;
  }

  /**
   * Checks if the panel is display-only or user modify-able
   * 
   * @return <code>true</code> if display-only, otherwise <code>false</code>
   */
  public boolean isDisplayOnly()
  {
    return displayOnly;
  }

  /**
   * Sets the panel to be display-only or user modify-able
   * 
   * @param newDisplayOnly <code>true</code> will set display-only, while
   *          <code>false</code> will set the panel to user modify-able.
   */
  public void setDisplayOnly(boolean newDisplayOnly)
  {
    displayOnly = newDisplayOnly;
  }

  /**
   * Adds a <code>ValidUserMoveListener</code> to listen for user moves.
   * 
   * @param listener ValidMoveListener
   */
  public void addValidUserMoveListener(ValidUserMoveListener listener)
  {

    this.validUserMoveListener = listener;
  }

  public void update(NodeState nodeState)
  {
    this.nodeState = nodeState;
    update();
  }

  abstract public void update();
}