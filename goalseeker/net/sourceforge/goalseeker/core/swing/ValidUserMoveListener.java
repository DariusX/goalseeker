package net.sourceforge.goalseeker.core.swing;

import net.sourceforge.goalseeker.core.NodeState;

/**
 * A ValidUserMoveListener listens for a valid user operation to be performed in the
 * FifteenPanel. Performing a valid move might require some other changes,
 * so the container for the FifteenPanel will usually implement this
 * listener.
 *
 * @author: Darius Cooper
 */
public interface ValidUserMoveListener {
/**
 * This method is invoked when a valid move is performed in the FifteenPanel
 * It is only invoked for user-initiated moves that are directed at the FifteenPanel
 * not for program-initiated moves.
 *
 * So, if a user action (e.g. mouse click) does not result in a valid move
 * this method is not triggered.
 * If the state of the Fifteen panel is changed programmatically (e.g. while displaying
 * a possible solution) then too this method is not invoked.
 *
 * It will be invoked if a user action (e.g. mouse click)  results in a valid operation
 *
 * @param event aa.display.ValidUserMoveEvent
 */
//void validUserMovePerformed(ValidUserMoveEvent event);

void validUserMovePerformed(NodeState newState);
}
