package net.sourceforge.goalseeker.core;
/**
 * This <code>abstract</code> class must be extended by a sub-class that
 * represents a "state". A state is one possible route that can be reached from
 * the original starting point (or "starting state").
 * 
 * The <code>NodeState</code> does *not* need to incorporate information about
 * the position of the state within the search network. This task is performed
 * by the <code>SearchNode</code>. (Each <code>SearchNode</code>
 * incorporates one <code>NodeState</code>.
 * 
 * The most function of the sub-classes of <code>NodeState</code> is to
 * "expand" the node and return a set of states that can be reached in a single
 * step from the current state.
 * 
 * @author: Darius Cooper
 */
abstract public interface NodeState extends  Comparable, Cloneable
{
  
  /**
   * This class represents an exception that is thrown when an operation is
   * attempted on a <code>NodeState</code> but that operation is not allowed
   * for the current state of the node.
   */
  public static class InvalidOperationException extends Exception
  {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public InvalidOperationException()
    {
    }
    
    public InvalidOperationException(String message)
    {
      super(message);
    }
  }
  /**
   * Abstract method that must be implemented by <code>NodeState</code>
   * sub-classes. Indicates if this is a goal state (i.e. a solution state)
   * 
   * @return <code>true</code> if this is a goal state, else return
   *         <code>false</code>
   */
  abstract public boolean isGoalState();
  // Cost to come to this <code>NodeState</code> from the previous
  // <code>NodeState</code>


  /**
   * Returns the cost to come to this <code>NodeState</code> from the previous
   * <code>NodeState</code>. This cost is set when the <code>NodeState</code>
   * is created (in the <code>getNextNodeStateList()</code> method.
   */
  abstract public int getCostFromPrev();

  /**
   * Abstract method that must be implemented by <code>NodeState</code>
   * sub-classes. The implementation should return an array of SearchNodes
   */
  abstract public NodeState[] getNextNodeStateList();

  /**
   * Sets the cost to come to this <code>NodeState</code> form the preceding
   * <code>NodeState</code>
   */
  abstract public void setCostFromPrev(int newCostFromPrev);

  /**
   * Shuffles the current state, by making a certain number of random
   * operations. Implementing classes may attempt to remove some "cycles", but
   * this is not a requirement. The default implementation of this method does
   * nothing.
   * 
   * @param times    
   */
 abstract public void shuffle(int times);
 
 abstract public Object clone();
}