package net.sourceforge.goalseeker.core;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


/**
 * <p>
 * Puzzle15State
 * </p>
 * <p>
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * 
 * <p>
 * Date: Aug 25, 2004
 * </p>
 * 
 * @author Darius Cooper
 * @version 1.0
 */
public class PuzzleService 
{
  // The <code>Searcher</code> used to find a solution to the puzzle.
  protected Searcher searcher = null;

  protected int currentStep = 0;
  // A counter showing the step/move number; it is incremented with each move
  protected int playerMoveIdx = 0;

  // A stack that holds moves made by the user. This is used to UN-do a move
  protected Stack playerMovesStack;
  protected String displayedMoveNum = "0";
  
  private NodeState currentNodeState;

  
  public void setUpInitialState(NodeState initialNodeState, Heuristic heuristic)
  {
    currentNodeState = initialNodeState;
    
    //Send a copy to the searcher
    SearchNode startNode = new SearchNode((NodeState)initialNodeState.clone());

    // If we already have a search thread, we want to re-use it
    // This is important when this runs as an Applet, so that
    // it does not have to have permission for change the ThreadGroup
    if (getSearcher() == null)
    {
      setSearcher(new Searcher(startNode, heuristic));
      getSearcher().setDaemon(true);
      getSearcher().start();
    }
    else
    {
      getSearcher().setStartNode(startNode);
      getSearcher().solveFromNewStartNode();
    }
  }
  
  
  public void stopSolving()
  {
    getSearcher().setStopRequested(true);
  }

  
  public int getCurrentStep()
  {
    return currentStep;
  }
  
  
  public int getPlayerMoveIdx()
  {
    return playerMoveIdx;
  }
  

  /**
   * @param node
   * 
   */
  public void handleResetCount()
  {
    playerMoveIdx = 0;
    setPlayerMovesStack(new Stack());
    if (currentNodeState != null)
    {
      playerMovesStack.push(currentNodeState);
    }
  }
  


  private Searcher getSearcher()
  {
    return searcher;
  }
  
  private Stack getPlayerMovesStack()
  {
    return playerMovesStack;
  }

  private void setPlayerMovesStack(Stack playerMovesStack)
  {
    this.playerMovesStack = playerMovesStack;
  }



  private void setSearcher(Searcher searcher)
  {
    this.searcher = searcher;
  }


  public NodeState getCurrentNodeState()
  {
    return  currentNodeState;
  }

//  @Override
//  public void validUserMovePerformed(ValidUserMoveEvent event)
//  {
//    NodeState newNodeState =  (NodeState)(event.getNewState().clone()) ;
//    playerMoveIdx++;
//    getPlayerMovesStack().push(newNodeState);
//  }

  public void validUserMovePerformed(NodeState newNodeState)
  {
    playerMoveIdx++;
    getPlayerMovesStack().push(newNodeState);
  }
  

  public void handleUndo()
  {
    playerMoveIdx--;
    if (playerMovesStack.size() >= 2)
    {
      playerMovesStack.pop(); // Discard top state
      currentNodeState = (NodeState) ((NodeState)playerMovesStack.peek()).clone();
      
    }
    if (playerMoveIdx < 0)
    {
      displayedMoveNum = "0";
    }
    else
    {
      displayedMoveNum = Integer.toString(playerMoveIdx);
    }
  }

  /**
   * 
   */
  public void handleNextStep()
  {
    if (currentNodeState.isGoalState())
    {
      return;
    }
    currentStep++;
    currentNodeState = (NodeState) (getSearcher().getSolutionNodes()[currentStep].getNodeState()
        .clone());
    displayedMoveNum = Integer.toString(currentStep);
  }

  /**
   * 
   */
  public List<NodeState> retrieveSolution()
  {
    playerMoveIdx = 0;
    setPlayerMovesStack(new Stack());
    playerMovesStack.push(currentNodeState);
    currentStep = 0;
    displayedMoveNum = "0";
    
    waitForSearchCompletion();
    if (getSearcher()==null)
    {
      System.out.println("ERROR: Searcher is null");
    }
    SearchNode [] searchNodes = getSearcher().getSolutionNodes();
    if (searchNodes==null)
    {
      System.out.println("ERROR: Solution nodes are null");
    }
    if (searchNodes[0]==null)
    {
      System.out.println("ERROR: Solution node[0] is null");
    }
    currentNodeState = searchNodes[0].getNodeState();
    List<NodeState> theSolution = new ArrayList<NodeState>();
    for (SearchNode nextSearchNode : searchNodes)
    {
      theSolution.add(nextSearchNode.getNodeState());
    }
    return theSolution;
  }

  public String getDisplayedMoveNum()
  {
    return displayedMoveNum;
  }

  /**
   * 
   */
  public void waitForSearchCompletion()
  {
    while (isBusySearching())
    {
      try
      {
        Thread.sleep(1000);
      }
      catch (InterruptedException ex)
      {
      }
    }
    try
    {
      Thread.sleep(2000);
    }
    catch (InterruptedException ex)
    {
    }
  }

  /**
   * @return
   */
  public boolean isBusySearching()
  {
    return (getSearcher() != null) && (getSearcher().isStartRequested() && getSearcher().getSolutionNodes()==null);
  }
}
