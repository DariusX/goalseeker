package net.sourceforge.goalseeker.core;

import java.util.HashMap;
import java.util.TreeSet;
/**
 * This class performs the search for a goal-state. 
 * Since searching can take time, it extends Thread and can therefore 
 * easily be run as a separate thread.
 */

public class Searcher extends IteratorThread
{

	public static int SOLUTION_TYPE_SINGLE = 1;
	public static int SOLUTION_TYPE_MULTIPLE = 2;

	private int solutionType = SOLUTION_TYPE_SINGLE;

	/* An array of search nodes, 
	   position zero has the start node
	   the last position has the solution node
	 */
	private SearchNode[] solutionNodes;
	//A copy of the start node that is passed in by the caller
	private SearchNode startNode;
	private Heuristic heuristic; //The heuristic to be used in the search
	private boolean debugTrace = false; //True means: write trace-info to console
	/**
	 * Searcher constructor
	 * @param startNode Any valid <code>SearchNode</code> that should be used as a starting point
	 * @param heuristic A heuristic that is valid for the type of startNode that
	 * has been provided. Note that the solutionState is not provided as that is
	 * encapsulated by the heuristic.
	 * The search calls <code>Thread.yield()</code> at every node, so that other threads
	 * remain responsive. At each such point, it also checks if some other thread has
	 * requested the search to stop.
	 */
	public Searcher(SearchNode startNode, Heuristic heuristic)
	{
		super();
		this.startNode = startNode;
		this.heuristic = heuristic;
	}
	/**
	 * Returns an array of <code>SearchNode</code> objects. 
	 * The zero-th position contains the start node, the last position contains 
	 * the solution node. 
	 * The positions in between are the states that lead from start-node to solution node.
	 * @return aa.node.SearchNode[] containing a solution, or
	 *         <code>null</code> is the solution has not yet been found.
	 */
	public SearchNode[] getSolutionNodes()
	{
		return solutionNodes;
	}
	/**
	 * Check if debug-tracing is on.
	 * @return <code>true</code> is tracing is on, else return <code>false</code>.
	 */
	public boolean isDebugTrace()
	{
		return debugTrace;
	}
	/**
	 * This method is use to execute the search
	 * In this version, that is all it does.
	 */
	public void runIteration()
	{

		search();
		System.out.println("Searcher search() method ending");

	}

	private void search()
	{
		if (this.solutionType == SOLUTION_TYPE_SINGLE)
		{
			searchSinglePath();
		}
		else if (this.solutionType == SOLUTION_TYPE_MULTIPLE)
		{
			searchMultiplePath();
		}
		else
		{
			if (this.isDebugTrace())
			{
				System.out.println(
					"Solution type is invalid: should be "
						+ "SOLUTION_TYPE_SINGLE or SOLUTION_TYPE_MULTIPLE");
			}
		}
	}
	/**
	 * Searches for an optimal solution from the start state to the solution.
	 * If debug-trace has been set to <code>true</code>, then each node that is
	 * examined is written using <code>System.out.println()</code> to allow for
	 * debugging.
	 * When this method completes successfully, it will update the solution-set
	 * array, which can then be accessed by a call to <code>getSolutionSet()</code>
	 * 
	 */
	private void searchSinglePath()
	{

		//Create a new copy a start node, using only the state from the input
		startNode = new SearchNode(startNode.getNodeState());

		solutionNodes = null;

		HashMap stateMap = new HashMap();
		TreeSet nodeList = new TreeSet();
		int nodeListMin = Integer.MAX_VALUE;

		SearchNode currentNode = startNode;
		//Place Start node in the Pool
		stateMap.put(currentNode.getNodeState(), currentNode);
		nodeList.add(currentNode);

		int nodeNumber = 0;
		while (!currentNode.isGoalState())
		{
			Thread.yield();
			if (isStopRequested())
			{
				solutionNodes = null;
				return;
			}
			nodeNumber++;
			if (this.isDebugTrace())
			{
				System.out.println("Current " + currentNode);
			}
			if (currentNode.isExpanded())
			{
				solutionNodes = null;
				System.out.println("Cannot find a solution!");
				break;
			}
			nodeList.remove(currentNode);
			//currentNode.setExpanded(true);
			SearchNode[] expandedNodes = currentNode.getNextNodeList();
			//int expandedListMin = Integer.MAX_VALUE;
			SearchNode expandedMinNode = null;

			for (int i = 0; i < expandedNodes.length; i++)
			{
				if (expandedNodes[i] == null)
				{
					break; //Handles situation where the returned array 
					//is not completely filled
				}
				Thread.yield();
				if (this.isDebugTrace())
				{
					System.out.println(
						"                            Expanded # "
							+ i
							+ "= "
							+ expandedNodes[i]);
				}
				NodeState currentNodeState = expandedNodes[i].getNodeState();
				SearchNode existingSearchNode =
					(SearchNode) stateMap.get(currentNodeState);
				if (existingSearchNode != null)
				{
					if (existingSearchNode.getLowestCost()
						<= expandedNodes[i].getLowestCost())
					{
						//Ignore the current route to this node if we already 
						//visited this node and have not
						//found a lower-cost route to reach this node
						continue;
					}
					else
					{
						//No need to recalculate the heuristic
						expandedNodes[i].setEstimateToGoal(
							existingSearchNode.getEstimateToGoal());
						//Remove the node from the pool (it will be replaced by the new one)
						stateMap.remove(currentNodeState);
						nodeList.remove(existingSearchNode);
						//What else? Change anything about the costs ...back-tracking?
					}
				}
				else
				{
					//Calculate the heuistic for the newly expanded node
					expandedNodes[i].setEstimateToGoal(
						heuristic.getEstimatedCostToGoal(expandedNodes[i]));
				}
				//Calc Tot Est
				int currentTotEst =
					expandedNodes[i].getLowestCost()
						+ expandedNodes[i].getEstimateToGoal();
				if (currentTotEst < nodeListMin)
				{
					nodeListMin = currentTotEst;
					expandedMinNode = expandedNodes[i];
				}
				//Place in Pool
				stateMap.put(currentNodeState, expandedNodes[i]);
				nodeList.add(expandedNodes[i]);

			}
			if (expandedMinNode != null)
			{ //Found something lower than the pool
				currentNode = expandedMinNode;
			}
			else
			{
				//Backtrack..re-setting the costs
				while (currentNode.getPrevSearchNode() != null)
				{
					currentNode.getPrevSearchNode().setEstimateToGoal(
						currentNode.getLowestCost()
							- currentNode.getPrevSearchNode().getLowestCost()
							+ currentNode.getEstimateToGoal());
					currentNode = currentNode.getPrevSearchNode();
				}
				//Select the minimum from the pool
				try
				{
					currentNode = (SearchNode) nodeList.first();
				}
				catch (java.util.NoSuchElementException e)
				{
					System.out.println("No such element");
				}
			}
			//break;
		}

		//Backtrack.. to Print result
		System.out.println(
			"========================================================");
		solutionNodes = new SearchNode[currentNode.getLowestCost() + 1];
		for (int i = solutionNodes.length - 1; currentNode != null;)
		{
			if (this.isDebugTrace())
			{

				System.out.println(currentNode.toString() + i);
			}
			solutionNodes[i] = currentNode;
			i--;
			currentNode = currentNode.getPrevSearchNode();
		}

	}

	private void searchMultiplePath()
	{

		//TODO
	}
	/**
	 * Sets debug tracing on or off.
	 * @param newDebugTrace Set to <code>true</code> to start tracing, or
	 * set to <code>false</code> to turn it off.
	 */
	public void setDebugTrace(boolean newDebugTrace)
	{
		debugTrace = newDebugTrace;
	}
	/**
	 * Set a new start node to be exploer during a search.
	 * 
	 * @param newStartNode A new start node from where the search should begin
	 */
	public void setStartNode(SearchNode newStartNode)
	{

		this.startNode = new SearchNode(newStartNode.getNodeState());

	}
	/**
	 * Starts a new solution, using the start node provided in the call to
	 * <code>setStartNode()</code>
	 */
	public void solveFromNewStartNode()
	{
		this.solutionNodes = null;
		setStartRequested(true);
	}
	/**
	 * Sets the solutionType.
	 * @param solutionType The solutionType to set
	 */
	public void setSolutionType(int solutionType)
	{
		this.solutionType = solutionType;
	}

}
