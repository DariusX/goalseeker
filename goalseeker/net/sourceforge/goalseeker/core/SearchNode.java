package net.sourceforge.goalseeker.core;
/**
 * This class represents a 'point' or 'node' along a path being searched Such a
 * node may lie on the optimal path from the start to the goal, or it may lie on
 * a 'costlier' path, or it may not lie on any reasonable path to the goal.
 * 
 * The actual state is represented by the <code>NodeState</code> class. Each
 * <code>SearchNode</code> contains a <code>NodeState</code>. In addition,
 * it also has information about the least-cost means of reaching that node, the
 * estimate of the cost to go from the node to the solution, and the information
 * about the preceding node (on the path from the start-node.
 * 
 * In essence, while the <code>NodeState</code> represents state 'as is', the
 * <code>SearchNode</code> represents the state as it exists along the search
 * path, with the information required to perform the search.
 * 
 * @author: Darius Cooper
 */
public class SearchNode implements Comparable
{
  // The node state encapsulated by this <code>SearchNode</code>
  private NodeState nodeState = null;
  private int estimateToGoal = 0; // Estimated cost to reach the goal
  /**
   * Whether this node has been expanded. Expanding a node means figuring out
   * the possible next steps that are "one step" away from this node.
   */
  private boolean expanded;
  /*
   * The lowest currently known cost to get from the startNode to this node.
   */
  private int lowestCost = 0;
  /**
   * The <code>SearchNode</code> that is "one step" before this
   * <code>SearchNode</code>, when one takes the current least-cost path from
   * the start-node to this node. If this is the start-node, this remains as
   * <code>null</code>
   */
  private SearchNode prevSearchNode = null;

  /**
   * Creates a <code>SearchNode</code> to encapsulate the
   * <code>NodeState</code>
   * 
   * @param nodeState
   *          The <code>NodeState</code> to be encapsulated by this
   *          <code>SearchNode</code>
   */
  public SearchNode(NodeState nodeState)
  {
    this.nodeState = nodeState;
  }

  /**
   * Compares two <code>SearchNode</code> objects. A node that is estimated to
   * be a better prospect to explore to reach the solution-state is considered
   * to be greater than a node that is estimated to be a lesser candidate. The
   * rules used are as follow: Firstly, all expanded nodes are less than (i.e.
   * further away from the solution) than all unexpanded nodes. Secondly, we
   * calculate a current estimate of totalCost via each node. The estimated
   * total cost is the sum of: the current lowest know path from start to this
   * node, and an estimate of the distance from this node to the solution state.
   * If these two rules yield a tie, then a node that has a smaller estimate of
   * distance to solution-state is considered to be the greater (better
   * candidate). Finally, if all the rules tie, the <code>NodeState</code> of
   * each node is compared. In most situations, this last step will yield a
   * arbitrarily-chosen tie-breaker.
   * 
   * @return Follows the convention used by the <code>Comparable</code>
   *         interface
   * @param obj
   *          <code>SearchNode</code> to be compared against.
   */
  public int compareTo(Object obj)
  {
    if (this.isExpanded() && !((SearchNode) obj).isExpanded())
    {
      return +1;
    }
    else if (!this.isExpanded() && ((SearchNode) obj).isExpanded())
    {
      return -1;
    }
    int thisEstimateTotal = getEstimateToGoal() + getLowestCost();
    int otherEstimateTotal = ((SearchNode) obj).getEstimateToGoal()
        + ((SearchNode) obj).getLowestCost();
    if (thisEstimateTotal < otherEstimateTotal)
    {
      return -1;
    }
    else if (thisEstimateTotal > otherEstimateTotal)
    {
      return +1;
    }
    else
    {
      if (getEstimateToGoal() > ((SearchNode) obj).getEstimateToGoal())
      {
        return -1;
      }
      else if (getEstimateToGoal() < ((SearchNode) obj).getEstimateToGoal())
      {
        return +1;
      }
      else
      {
        return getNodeState().compareTo(((SearchNode) obj).getNodeState());
      }
    }
  }

  /**
   * Expands the current <code>SearchNode</code> by getting the nodes that are
   * "one step away" (or, "one operation away"). This method relies on the
   * <code>NodeState</code> object to figure out the next set of states. The
   * method <code>getNextNodeStateList()</code> must be implemented by the
   * specific <code>NodeState</code> class.
   * 
   * @return An array of <code>SearchNode</code> objects that represent the
   *         set of nodes that can be reached in one operation from the current
   *         node.
   */
  public SearchNode[] getNextNodeList()
  {
    NodeState[] nodeStates = nodeState.getNextNodeStateList();
    SearchNode[] searchNodes = new SearchNode[nodeStates.length];
    for (int i = 0; i < searchNodes.length; i++)
    {
      if (nodeStates[i] != null)
      {
        searchNodes[i] = new SearchNode(nodeStates[i]);
        searchNodes[i].setPrevSearchNode(this);
        searchNodes[i].setLowestCost(this.getLowestCost()
            + searchNodes[i].getNodeState().getCostFromPrev());
      }
    }
    return searchNodes;
  }

  /*
   * Check if this node is a solution. @return <code>true </code> if the <code>NodeState</code>
   * encapsulated by this <code>SearchNode</code> is a "goal" or "solution"
   * state, else <code>false</code>
   */
  public boolean isGoalState()
  {
    return nodeState.isGoalState();
  }

  /**
   * Returns the current lowest estimate to get from this
   * <code>SearchNode</code> to a final solution.
   * 
   * @return Estimated cost from current state to goal.
   */
  public int getEstimateToGoal()
  {
    return estimateToGoal;
  }

  /**
   * Returns the current lowest known cost to get from to this
   * <code>SearchNode</code> from the start-node.
   * 
   * @return Cost to get from start state to the current node.
   */
  public int getLowestCost()
  {
    return lowestCost;
  }

  /**
   * Returns the <code>NodeState</code> that is encapsulated by this
   * <code>SearchNode</code>
   * 
   * @return The <code>NodeState</code> that is encapsulated by this
   *         <code>SearchNode</code>
   */
  public NodeState getNodeState()
  {
    return nodeState;
  }

  /**
   * Returns the <code>SearchNode</code> that is "one step" before this
   * <code>SearchNode</code>, when one takes the current least-cost path from
   * the start-node to this node. If this is the start-node, returns
   * <code>null</code>
   * 
   * @return The previous <code>SearchNode</code>
   */
  public SearchNode getPrevSearchNode()
  {
    return prevSearchNode;
  }

  /**
   * Returns an indication of whether the search has expanded this node to
   * compute the states that can be reached by one "step" or "operation" from
   * this <code>SearchNode</code>
   * 
   * @return <code>true </code> is it has been expanded, else
   *         <code>false</code>
   */
  public boolean isExpanded()
  {
    return expanded;
  }

  /**
   * Sets the estimate of the cost to get from this <code>SearchNode</code> to
   * the solution node. This is usually calculated by the <code>Hueristic</code>
   * 
   * @param newEstimatedCostToGoal
   * 
   */
  public void setEstimateToGoal(int newEstimateToGoal)
  {
    estimateToGoal = newEstimateToGoal;
  }

  /**
   * Mark this <code>SearchNode</code> as having been expanded. This is
   * ordinarily called only by the search method(s).
   * 
   * @param newExpanded
   *          <code>true</code> means it has been expanded.
   */
  public void setExpanded(boolean newExpanded)
  {
    expanded = newExpanded;
  }

  /**
   * Set the lowest known cost of getting from the start state to this
   * <code>SearchNode</code> This is ordinarily called only by the search
   * method(s).
   * 
   * @param newLowestCost
   *          The lowest known cost.
   */
  public void setLowestCost(int newLowestCost)
  {
    lowestCost = newLowestCost;
  }

  /**
   * Sets a reference to the <code>SearchNode</code> that is one step "before"
   * this node in the path from the start-node. This is ordinarily called only
   * by the search method(s).
   * 
   * @param newPrevSearchNode
   *          The previous <code>SearchNode</code>
   */
  public void setPrevSearchNode(SearchNode newPrevSearchNode)
  {
    prevSearchNode = newPrevSearchNode;
  }

  /**
   * Returns a String Representation of the <code>SearchNode</code> It relies
   * on the <code>toString()</code> method of the encapsulated
   * <code>NodeState</code> to get a representation of the state. It prefixes
   * the state with three numbers: the estimate to the goal, the lowest cost to
   * reach the node, and the sum of the two.
   * 
   * @return A string representation of the <code>SearchNode</code>
   */
  public String toString()
  {
    return ("(" + Integer.toString(getEstimateToGoal()) + "/"
        + Integer.toString(getLowestCost()) + "/"
        + Integer.toString((getLowestCost() + getEstimateToGoal())) + ") = " + nodeState
        .toString());
  }
}