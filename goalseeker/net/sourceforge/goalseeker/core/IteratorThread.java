package net.sourceforge.goalseeker.core;

/**
 * A type of thread that encapsulates a pattern where the thread does its "thing"
 * and then sleeps. Then, it is woken up at regular intervals to do its "thing" again.
 * Also supports the ability to request the intration to stop (but the individual
 * child classes must implement the checks for such a request).
 *
 * @author: Darius Cooper
 */
abstract public class IteratorThread extends Thread {

	private static long sleepTimeBetweenIterations = 2000;
		
	private boolean stopRequested=false; //Stop the iteration 
	private boolean startRequested= false; //Start an iteration true/false

/**
 * RepetitiveThread constructor .
 */
public IteratorThread() {
	super();
}
/**
 * RepetitiveThread constructor.
 * @param name java.lang.String
 */
public IteratorThread(String name) {
	super(name);
}
/**
 * RepetitiveThread constructor.
 * @param group java.lang.ThreadGroup
 * @param name java.lang.String
 */
public IteratorThread(ThreadGroup group, String name) {
	super(group, name);
}
/**
 * Gets the time the thread will sleep after one iteration before checking
 * checking for another iteration request.
 * @return long Time in milliseconds
 */
public static long getSleepTimeBetweenIterations() {
	return sleepTimeBetweenIterations;
}
/**
 * Checks if a start has been requested (either for the first time or
 * for a subsequent iteration). Allows a caller to check if an iteration
 * is in progress.
 *
 * @return boolean
 */
public boolean isStartRequested() {
	return startRequested;
}
/**
 * Checks if a caller has requested that the iteration should stop.
 *
 * @return boolean
 */
public boolean isStopRequested() {
	return stopRequested;
}
/**
 * Runs the first iteration. Then, sleeps and wakes and checks for another
 * start request. Does this continuously.
 */
public void run() {

	setStartRequested(true);

	for (;;) {
		if (isStartRequested()) {
			setStopRequested(false);
			runIteration();
			setStartRequested(false);
		}
		try {
			sleep(getSleepTimeBetweenIterations());
		} catch (InterruptedException ex) {
			//}

		}
	}
}
/**
 * This is like a run method, but when it ends the thread does not end.
 * Instead, the thread sleeps and this method is invoked when the thread
 * next receives a "start" request.
 */
public abstract void runIteration();
/**
 * Gets the time the thread will sleep after one iteration before checking
 * checking for another iteration request.
 *
 * @param newSleepTimeBetweenIterations long Time in milliseconds
 */
public static void setSleepTimeBetweenIterations(long newSleepTimeBetweenIterations) {
	sleepTimeBetweenIterations = newSleepTimeBetweenIterations;
}
/**
 * Requests a start of an iteration.
 *
 * @param newStartRequested boolean
 */
public void setStartRequested(boolean newStartRequested) {
	startRequested = newStartRequested;
}
/**
 * Requests that the thread iteration should stop.
 *
 * @param newStopRequested boolean
 */
public void setStopRequested(boolean newStopRequested) {
	stopRequested = newStopRequested;
}
}
