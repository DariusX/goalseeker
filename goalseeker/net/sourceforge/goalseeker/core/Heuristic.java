package net.sourceforge.goalseeker.core;

/**
 * This <code>interface</code> must be implemented by any class that wants to
 * calculate a heuristic function that estimates the distance from
 * a given state to a goal state.
 *
 * The current design assume that the goal-state is known to the
 * class that implements the <code>Heuristic</code> <code>interface</code>
 *
 * @author: Darius Cooper
 */
public interface Heuristic {
/**
 * Estimates the cost to go from a specified <code>SearchNode</code> to a goal-state. 
 * @return Estimated cost (represented as a positive integer. A cost of ZERO implies
 * that one is at the goal.
 * @param currentNode The node from which the distance to the goal is to be estimated.
 */
int getEstimatedCostToGoal(SearchNode currentNode);
}
